from twilio.rest import Client
from kmsg.contrib.backends.base import KmsgBackend
from kmsg.dclasses import Response, SmsMessageData

from kommons.utils.generic import decify


class TwilioSMSBackend(KmsgBackend):

    def _get_client(self):
        gateway_dict = self.setting.settings.get('gateway', {})
        account_sid = gateway_dict.get('account_sid')
        auth_token = gateway_dict.get('auth_token')
        return Client(account_sid, auth_token)

    def send_message(self, message_data: SmsMessageData, **kwargs) -> Response:
        try:
            client = self._get_client()

            twilio_message = client.messages.create(
                body=message_data.message,
                from_=message_data.sender,
                to=message_data.recipients[0] if message_data.recipients else ''
            )
            is_valid = True
            resp_code = 0
            message = 'SMS sent successfully'
            ref_code = twilio_message.sid
            unit_cost = twilio_message.price
            currency_code = twilio_message.price_unit
        except Exception as e:
            is_valid = False
            resp_code = getattr(e, 'smtp_code', 999)
            message = str(e)
            ref_code = ''
            unit_cost = 0
            currency_code = ''

        return Response(is_valid=is_valid, message=message, code=str(resp_code), ref_code=ref_code,
                        unit_cost=decify(unit_cost), currency_code=currency_code)
