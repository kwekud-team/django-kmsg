import requests
from kmsg.contrib.backends.base import KmsgBackend
from kmsg.dclasses import Response, SmsMessageData

from kommons.utils.generic import decify


class HubtelSMSBackend(KmsgBackend):

    def send_message(self, message_data: SmsMessageData, **kwargs) -> Response:
        is_sent, error_msg, json_response = self._send_response(message_data)

        is_valid, resp_code, unit_cost = True, 999, 0
        if is_sent:
            resp_code = json_response.get('status')
            is_valid = resp_code == 0
            unit_cost = json_response.get('rate')

        if is_valid:
            is_valid = True
            message = json_response.get('statusDescription') or 'SMS sent successfully'
            ref_code = json_response.get('messageId', '')
        else:
            is_valid = False
            message = error_msg or json_response.get('statusDescription') or 'SMS not sent'
            ref_code = ''

        return Response(is_valid=is_valid, message=message, code=str(resp_code), ref_code=ref_code,
                        unit_cost=decify(unit_cost), currency_code='GHS')

    def _send_response(self, message_data):
        gateway_dict = self.setting.settings.get('gateway', {})
        api_url = gateway_dict.get('api_url')
        auth = (gateway_dict.get('clientid'), gateway_dict.get('clientsecret'))

        to = message_data.recipients[0] if message_data.recipients else ''
        payload = {
            'from': message_data.sender,
            'to': to if to.startswith('+') else f'+{to}',
            'content': message_data.message,
        }

        try:
            body = requests.post(api_url, auth=auth, json=payload).json()
            error_msg = ''
        except Exception as e:
            body = {}
            error_msg = str(e)

        is_sent = error_msg == ''
        return is_sent, error_msg, body
