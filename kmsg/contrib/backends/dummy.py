import logging
from kmsg.contrib.backends.base import KmsgBackend
from kmsg.dclasses import Response, EmailMessageData, SmsMessageData
from kmsg.constants import KmsgK


class DummyBackend(KmsgBackend):

    def send_message(self, message_data: EmailMessageData, **kwargs) -> Response:
        recipient = message_data.recipients[0]

        if recipient.startswith(KmsgK.TEST_SUCCESS_PREFIX.value):
            is_valid = True
            resp_code = 0
            message = 'SMS sent successfully'
            logging.info(f"DummyBackend.send_message: {resp_code}: {message}")
        else:
            is_valid = False
            resp_code = 999
            message = 'SMS not sent'
            logging.error(f"DummyBackend.send_message: {resp_code}: {message}")
        return Response(is_valid=is_valid, message=message, code=str(resp_code))


class DummySMTPBackend(DummyBackend):
    pass


class DummySMSBackend(DummyBackend):
    pass
