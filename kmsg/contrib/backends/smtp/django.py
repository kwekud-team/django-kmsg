from django.core import mail
from django.core.mail import EmailMultiAlternatives

from kmsg.contrib.backends.base import KmsgBackend
from kmsg.dclasses import Response, EmailMessageData
from kmsg.constants import KmsgK


class DjSMTPBackend(KmsgBackend):

    def get_connection(self):
        gateway_dict = self.setting.settings.get('gateway', {})

        kw = {
            'host': gateway_dict.get('host'),
            'port': gateway_dict.get('port'),
            'username': gateway_dict.get('user'),
            'password': gateway_dict.get('password'),
            'use_ssl': gateway_dict.get('use_ssl')
        }
        return mail.get_connection(self.backend_path, **kw)

    @staticmethod
    def map_to_fields(message_data):
        return {
            'to': message_data.recipients,
            'from_email': message_data.sender,
            'cc': message_data.cc,
            'bcc': message_data.bcc,
            'subject': message_data.subject,
            'body': message_data.message,
        }

    def send_message(self, message_data: EmailMessageData, **kwargs) -> Response:
        fields = self.map_to_fields(message_data)
        msg = EmailMultiAlternatives(**fields, connection=self.get_connection())

        attachments = self.get_attachments()
        for f in attachments:
            msg.attach(f['filename'], f['content'], f['content_type'])

        try:
            msg.send()
            is_valid = True
            resp_code = 0
            message = 'Email sent successfully'
        except Exception as e:
            is_valid = False
            resp_code = getattr(e, 'smtp_code', 999)
            message = e.smtp_error.decode("utf-8") if hasattr(e, 'smtp_error') else str(e)
        return Response(is_valid=is_valid, message=message, code=str(resp_code))

    # TODO: implement getting attachments
    @staticmethod
    def get_attachments():
        return []

    
class DjConsoleBackend(DjSMTPBackend):
    backend_path = KmsgK.DJ_MAIL_CONSOLE.value


class DjEmailBackend(DjSMTPBackend):
    backend_path = KmsgK.DJ_MAIL_SMTP.value
