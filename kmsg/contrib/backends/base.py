from kmsg.dclasses import Response, BaseMessageData


class KmsgBackend:
    backend_path = None

    def __init__(self, setting):
        self.setting = setting

    def send_message(self, message_data: BaseMessageData, **kwargs) -> Response:
        raise NotImplementedError()
