import logging
from copy import copy

from django.utils.module_loading import import_string

from kmsg.validators import is_valid_phone, is_valid_email
from kommons.utils.generic import decify
from kommons.utils.model import get_str_from_model
from kmsg.models import Template, Setting, MessageLog
from kmsg.dclasses import Response, BaseMessageData
from kmsg.constants import KmsgK
from kmsg.contrib.backends.base import KmsgBackend


class BaseDispatch:

    def __init__(self, site):
        self.site = site

    def get_setting(self, template):
        if template:
            return Setting.objects.filter(
                site=self.site,
                medium=template.medium,
                is_active=True
            ).order_by('priority').first()

    def get_template(self, identifier):
        return Template.objects.filter(
            site=self.site,
            identifier__iexact=identifier,
        ).first()

    def get_backend(self, setting) -> KmsgBackend:
        backend_path = setting.backend
        backend_class = import_string(backend_path)
        return backend_class(setting)

    def send(self, template_identifier, message_data: BaseMessageData, user, referrer=None,
             simulate_bulk=False, setting=None, **kwargs) -> Response:
        related_message = kwargs.get('related_message')
        relation_type = kwargs.get('relation_type')
        self.pre_send(**kwargs)

        template = self.get_template(template_identifier)
        setting = setting or self.get_setting(template)

        if not template:
            return Response.failure(message=KmsgK.DISPATCH_NO_TEMPLATE.value)

        if not setting:
            return Response.failure(message=KmsgK.DISPATCH_NO_SETTING.value)

        if setting.provider.validate_recipients:
            validate_resp = self._get_valid_recipients(setting.medium, message_data.recipients)
            if not validate_resp['all_valid']:
                response = Response.failure(message=KmsgK.DISPATCH_RECIPIENTS_INVALID.value)
                self.save_log(setting, template, message_data, response, referrer, user, related_message=related_message,
                              relation_type=relation_type)
                return response

        # Quick way to override message_data per setting. This is useful when retrying with different provider and we
        # need to change a property. eg: sender
        override_message_data = setting.settings.get('override_message_data', {})
        if override_message_data:
            for key, val in override_message_data.items():
                if hasattr(message_data, key):
                    setattr(message_data, key, val)

        backend = self.get_backend(setting)
        if simulate_bulk:
            return  self._send_in_bulk(backend, template, setting, referrer, user, message_data, **kwargs)

        response = self.do_send(backend, message_data, **kwargs)
        self.post_send(setting, template, message_data, response, referrer, user)

        message_log = self.save_log(setting, template, message_data, response, referrer, user,
                                    related_message=related_message, relation_type=relation_type)

        if not response.is_valid and setting.provider.retry_with_backup:
            next_setting = self._get_next_backup_provider(setting, template, provider=setting.provider.backup_provider)

            # Resend message with next provider
            self.send(template_identifier, message_data, user, referrer=referrer, simulate_bulk=simulate_bulk,
                      setting=next_setting, related_message=message_log, relation_type='retry', **kwargs)

        return response

    def _get_next_backup_provider(self, setting, template, provider=None):
        qs = Setting.objects.filter(
            site=setting.site,
            medium=template.medium,
            is_active=True,
        ).exclude(priority=setting.priority)

        if provider:
            qs = qs.filter(provider=provider)

        return qs.order_by('priority').first()

    def _get_valid_recipients(self, medium, recipients):
        valid_recipients = []

        validate_fn = {
            KmsgK.MEDIUM_SMS.value: is_valid_phone,
            KmsgK.MEDIUM_EMAIL.value: is_valid_email
        }.get(medium)

        if validate_fn:
            valid_recipients = [r for r in recipients if validate_fn(r)]

        return {
            'all_valid': len(valid_recipients) == len(recipients),
            'valid_recipients': valid_recipients
        }

    def _send_in_bulk(self, backend, template, setting, referrer, user, message_data, **kwargs):
        # Ideally, the backend should support sending bulk messages, so we send only 1 request. In cases where they
        # don't, we go through recipients and send each message. This is not efficient and should be used with caution.

        all_valid = True
        response_info = {'messages': [], 'codes': [], 'ref_codes': []}
        for x in message_data.recipients:
            single_message_data = copy(message_data)
            single_message_data.recipients = [x]

            response = self.do_send(backend, single_message_data, **kwargs)
            self.post_send(setting, template, single_message_data, response, referrer, user)

            if not response.is_valid:
                all_valid = False
            response_info['messages'].append(str(response.message))
            response_info['codes'].append(str(response.ref_code))
            response_info['ref_codes'].append(str(response.ref_code))

        return Response(
            is_valid=all_valid,
            message='|'.join(response_info['ref_codes']),
            code='|'.join(response_info['ref_codes']),
            ref_code='|'.join(response_info['ref_codes']),
        )

    def pre_send(self, **kwargs):
        pass

    def do_send(self, backend, message_data, **kwargs):
        return backend.send_message(message_data)

    def post_send(self, setting, template, message_data, response, referrer, user):
        pass

    def save_log(self, setting, template, message_data, response, content_object, user, **kwargs):
        content_str = get_str_from_model(content_object or self.site)
        content_pk = content_object.pk if content_object else self.site.pk

        related_message = kwargs.get('related_message')
        relation_type = kwargs.get('relation_type')

        return MessageLog.objects.create(
            site=self.site,
            content_str=content_str,
            content_pk=content_pk,
            setting=setting,
            template=template,
            response_is_valid=response.is_valid,
            response_code=response.code,
            response_message=response.message,
            reference=response.ref_code,
            message=message_data.message,
            sender=message_data.sender,
            recipients=','.join(message_data.recipients),
            created_by=user,
            modified_by=user,
            subject=getattr(message_data, 'subject', None),
            related_message=related_message,
            relation_type=relation_type,
            unit_cost=decify(response.unit_cost),
            currency_code=response.currency_code,
            # cc_list=','.join(message_data.cc),
            # bcc_list=','.join(message_data.bcc)
        )

    def build_template_data(self, template_identifier, context):
        template = self.get_template(template_identifier)

        if template:
            mapp = {
                KmsgK.MEDIUM_SMS.value: ['sender', 'message'],
                KmsgK.MEDIUM_EMAIL.value: ['subject', 'sender', 'message', 'cc', 'bcc']
            }
            dt = {}
            for key in mapp.get(template.medium, []):
                val = context.get(key, {})
                raw = getattr(template, key, '')
                try:
                    dt[key] = raw.format(**val)
                except Exception as e:
                    logging.error(f'{e}. key: {key}, val: {val}')
                    dt[key] = ''
            return dt


class Dispatcher(BaseDispatch):
    pass
