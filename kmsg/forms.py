from django import forms

from kmsg.constants import KmsgK
from kmsg.models import Setting, Template
from kmsg.settings import KMSG_SMS_BACKENDS, KMSG_EMAIL_BACKENDS


BACKENDS = (
    ('MAIL', (
        (KmsgK.BCK_MAIL_CONSOLE.value, 'Console'),
        (KmsgK.BCK_MAIL_DJANGO_SMTP.value, 'Django SMTP'),
        )
    ),
    ('SMS', (
        (KmsgK.BCK_SMS_HUBTEL.value, 'SMS HUBTEL'),
        (KmsgK.BCK_SMS_TWILIO.value, 'SMS TWILIO'),
        )
    ),
)


MEDIUMS = (
    (KmsgK.MEDIUM_EMAIL.value, 'Email'),
    (KmsgK.MEDIUM_SMS.value, 'Sms'),
)


class SettingAdminForm(forms.ModelForm):
    backend = forms.ChoiceField(choices=BACKENDS)
    medium = forms.ChoiceField(choices=MEDIUMS)

    class Meta:
        model = Setting
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['backend'].choices = self._get_backend_tuple()

    def _get_backend_tuple(self):
        medium_settings = {
            KmsgK.MEDIUM_EMAIL.value: KMSG_EMAIL_BACKENDS,
            KmsgK.MEDIUM_SMS.value: KMSG_SMS_BACKENDS,
        }

        xs = []
        for medium, backend_list in medium_settings.items():
            xx = [(x, x.split('.')[-1]) for x in backend_list]
            xs.append((medium.upper(), xx))
        return xs


class TemplateAdminForm(forms.ModelForm):
    medium = forms.ChoiceField(choices=MEDIUMS)

    class Meta:
        model = Template
        fields = '__all__'
