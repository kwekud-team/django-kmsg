from enum import Enum


class KmsgK(Enum):
    MEDIUM_EMAIL = 'email'
    MEDIUM_SMS = 'sms'

    # TEST_RECIPIENT_PREFIX = '__test__'
    TEST_SUCCESS_PREFIX = '__success__'
    TEST_FAILURE_PREFIX = '__failure__'

    BCK_MAIL_DUMMY = 'kmsg.contrib.backends.dummy.DummySMTPBackend'
    BCK_MAIL_CONSOLE = 'kmsg.contrib.backends.smtp.django.DjConsoleBackend'
    BCK_MAIL_DJANGO_SMTP = 'kmsg.contrib.backends.smtp.django.DjEmailBackend'
    BCK_SMS_DUMMY = 'kmsg.contrib.backends.dummy.DummySMSBackend'
    BCK_SMS_HUBTEL = 'kmsg.contrib.backends.sms.hubtel.HubtelSMSBackend'
    BCK_SMS_TWILIO = 'kmsg.contrib.backends.sms.twilio.TwilioSMSBackend'

    DJ_MAIL_CONSOLE = 'django.core.mail.backends.console.EmailBackend'
    DJ_MAIL_SMTP = 'django.core.mail.backends.smtp.EmailBackend'

    DISPATCH_NO_SETTING = 'No valid settings created for this medium. Please contact admins.'
    DISPATCH_NO_TEMPLATE = 'No valid template created for this medium. Please contact admins.'
    DISPATCH_RECIPIENTS_INVALID = 'Some of the recipients provided are not valid'

    EMAIL_SEND_SUCCESS = 'Email sent successfully'
    EMAIL_SEND_FAILED = 'Email sending failed'

    @classmethod
    def get_gateway(cls, backend):
        GATEWAY_MAP = {
            cls.MEDIUM_EMAIL.value: [cls.BCK_MAIL_CONSOLE.value, cls.BCK_MAIL_DJANGO_SMTP.value],
            cls.MEDIUM_SMS.value: [cls.BCK_SMS_HUBTEL.value]
        }
        gateway = ''
        for k, v in GATEWAY_MAP.items():
            if backend in v:
                gateway = k
                break
        return gateway

    # def get_mediums(self):

