from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
from django.template.defaultfilters import slugify

from kmsg.models import Provider, Setting, Template
from kmsg.constants import KmsgK
from kmsg.dclasses import EmailMessageData, SmsMessageData
from kmsg.contrib.dispatch.base import Dispatcher
from kmsg.validators import is_valid_phone


class BaseTestCase(TestCase):

    def setUp(self):
        self.site = self._create_site(domain='parent.testserver')
        self.user = self._create_user('test')

    def _create_site(self, domain, name=None):
        return Site.objects.create(domain=domain, name=name or domain)

    def _create_user(self, username, password='1234', is_active=True, **kwargs):
        return get_user_model().objects.create_user(username, password=password, is_active=is_active, **kwargs)


class MessageTestCase(BaseTestCase):

    def setUp(self):
        super().setUp()
        self.provider = self._create_provider('Test')
        self.dispatcher = Dispatcher(self.site)
        self.default_medium = KmsgK.MEDIUM_EMAIL.value

    def _create_provider(self, name, **kwargs):
        identifier = kwargs.get('identifier', slugify(name))
        created_by = kwargs.get('created_by', self.user)
        modified_by = kwargs.get('modified_by', self.user)
        return Provider.objects.create(
            site=self.site,
            identifier=identifier,
            name=name,
            created_by=created_by,
            modified_by=modified_by,
            **kwargs
        )

    def _create_setting(self, provider, template, priority, backend, **kwargs):
        created_by = kwargs.get('created_by', self.user)
        modified_by = kwargs.get('modified_by', self.user)
        return Setting.objects.create(
            site=self.site,
            provider=provider,
            medium=template.medium,
            backend=backend,
            priority=priority,
            created_by=created_by,
            modified_by=modified_by,
            **kwargs
        )

    def _create_template(self, medium, identifier, message, **kwargs):
        created_by = kwargs.get('created_by', self.user)
        modified_by = kwargs.get('modified_by', self.user)
        extra = kwargs.get('extra', {})
        return Template.objects.create(
            site=self.site,
            medium=medium,
            identifier=identifier,
            message=message,
            extra=extra,
            created_by=created_by,
            modified_by=modified_by,
            **kwargs
        )

    def test_create_setups(self):
        template = self._create_template(self.default_medium, 'test_ident', 'THis is a test message')

        # Two email settings
        self._create_setting(self.provider, template, priority=0, backend='email.backend.1')
        self._create_setting(self.provider, template, priority=1, backend='email.backend.2')

    def test_get_settings(self):
        template = self._create_template(self.default_medium, 'test_template', 'This is a test message')

        # No setting is created for medium
        setting = self.dispatcher.get_setting(template)
        self.assertIsNone(setting)

        mail1 = self._create_setting(self.provider, template, priority=0, backend='backend.1')
        self._create_setting(self.provider, template, priority=1, backend='backend.2')

        setting = self.dispatcher.get_setting(template)
        self.assertEqual(mail1, setting)

    def test_get_template(self):
        template = self.dispatcher.get_template('test_template')
        self.assertIsNone(template)

        template1 = self._create_template(self.default_medium, 'test_template', 'This is a test message')

        template = self.dispatcher.get_template('test_template')
        self.assertEqual(template1, template)

    def test_get_backend(self):
        template = self._create_template(self.default_medium, 'test_template', 'This is a test message')

        setting = self._create_setting(self.provider, template, priority=0, backend=KmsgK.BCK_MAIL_CONSOLE.value)
        backend = self.dispatcher.get_backend(setting)
        self.assertEqual(backend.backend_path, KmsgK.DJ_MAIL_CONSOLE.value)

    def _test_send_response(self, resp, is_valid, code, message):
        self.assertEqual(resp.is_valid, is_valid)
        self.assertEqual(resp.code, code)
        self.assertEqual(resp.message, message)


class EmailTestCase(MessageTestCase):

    def setUp(self):
        super().setUp()
        self.dispatcher = Dispatcher(self.site)

    def test_send_email(self):
        # Test send message
        message_data = EmailMessageData(sender='test@example.com', subject='Test subject', message='Test message',
                                        recipients=['test@example.com'])

        # Test no template
        resp = self.dispatcher.send('test_template', message_data, user=self.user)
        self._test_send_response(resp, is_valid=False, code='-1', message=KmsgK.DISPATCH_NO_TEMPLATE.value)
        # Create setting afterwards so we check for template
        template = self._create_template(KmsgK.MEDIUM_EMAIL.value, 'test_template', 'This is a test message')

        # No setting created
        resp = self.dispatcher.send('test_template', message_data, user=self.user)
        self._test_send_response(resp, is_valid=False, code='-1', message=KmsgK.DISPATCH_NO_SETTING.value)
        # Create setting afterwards so we check for template
        self._create_setting(self.provider, template, priority=0, backend=KmsgK.BCK_MAIL_CONSOLE.value)

        resp = self.dispatcher.send('test_template', message_data, user=self.user)
        self._test_send_response(resp, is_valid=True, code='0', message=KmsgK.EMAIL_SEND_SUCCESS.value)


class PhoneTestCase(MessageTestCase):

    def setUp(self):
        super().setUp()
        self.dispatcher = Dispatcher(self.site)

    def test_validate_before_send(self):
        message_data = SmsMessageData(sender='test@example.com',  message='Test message', recipients=['12345', '233201234567'])

        provider = self._create_provider("Pop", validate_recipients=True)
        template = self._create_template(KmsgK.MEDIUM_SMS.value, 'test_template', 'This is a test message')
        self._create_setting(provider, template, priority=0, backend=KmsgK.BCK_SMS_DUMMY.value)
        resp = self.dispatcher.send('test_template', message_data, user=self.user)

        self.assertFalse(resp.is_valid)


class ValidateTestCase(TestCase):
    DUMMY_RECORDS = [
        {"recipients": "23312345678", "response_code": "999"},
        {"recipients": "+23312345678", "response_code": "999"},
        {"recipients": "233987654321", "response_code": "0"},
    ]

    def test_validate_phone_numbers(self):
        dd = {'is_valid_and_999': [], 'not_valid_and_0': []}

        for obj in self.DUMMY_RECORDS:
            recipients = obj['recipients']
            is_valid = is_valid_phone(recipients)
            if obj['response_code'] == "0" and not is_valid:
                dd['not_valid_and_0'].append(recipients)

            if obj['response_code'] == "999" and is_valid:
                dd['is_valid_and_999'].append(recipients)
