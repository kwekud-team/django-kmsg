from kmsg.contrib.dispatch.base import Dispatcher
from kmsg.dclasses import BaseMessageData


def send_message(site, template_identifier, message_data: BaseMessageData, user, referrer=None, simulate_bulk=False,
                 **kwargs):
    kwargs['referrer'] = referrer
    return Dispatcher(site).send(template_identifier, message_data, user, simulate_bulk=simulate_bulk, **kwargs)


def build_template_data(site, template_identifier, context):
    return Dispatcher(site).build_template_data(template_identifier, context)