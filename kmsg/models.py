import uuid
from django.db import models
from django.conf import settings

from kmsg.constants import KmsgK
from kommons.utils.generic import get_dotted_dict


class BaseModel(models.Model):
    guid = models.UUIDField(blank=True)
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                                   related_name='%(app_label)s%(class)s_created_by')
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                                    related_name='%(app_label)s%(class)s_modified_by')

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.guid = self.guid or uuid.uuid4()
        super().save(*args, **kwargs)


class Provider(BaseModel):
    identifier = models.CharField(max_length=250)
    name = models.CharField(max_length=250)
    description = models.CharField(max_length=250, null=True, blank=True)
    retry_with_backup = models.BooleanField(default=False,
                                            help_text='If message should be sent using next available backup provider')
    backup_provider = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True,
                                        help_text='Preferred backup provider. If set, retries will only use this '
                                                  'provider, else it will pick next provider based on priority ASC')
    validate_recipients = models.BooleanField(default=False, help_text="Validated recipients are valid before sending. ")

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ['site', 'identifier']


class Setting(BaseModel):
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)
    medium = models.CharField(max_length=250)
    backend = models.CharField(max_length=250)
    settings = models.JSONField(default=dict, blank=True)
    priority = models.PositiveSmallIntegerField(default=1000)

    def __str__(self):
        return f'{self.provider}: {self.priority}: {self.medium}'
        # return f'{self.provider}: {self.backend}-{self.priority}-{self.medium}'

    class Meta:
        ordering = ('priority',)
        unique_together = [
            ('site', 'provider', 'medium', 'priority',),
            ('site', 'provider', 'backend',)
        ]


class Template(BaseModel):
    medium = models.CharField(max_length=50)
    identifier = models.CharField(max_length=250)
    message = models.TextField()
    sender = models.CharField(max_length=100, null=True, blank=True)
    subject = models.TextField(max_length=250, null=True, blank=True)
    extra = models.JSONField(default=dict, blank=True)
    estimated_message_count = models.PositiveSmallIntegerField(default=1)

    def __str__(self):
        return '%s: %s' % (self.medium, self.identifier)

    class Meta:
        unique_together = ('site', 'medium', 'identifier',)


class MessageLog(BaseModel):
    content_pk = models.CharField(max_length=100)
    content_str = models.CharField(max_length=100)

    setting = models.ForeignKey(Setting, on_delete=models.CASCADE)
    template = models.ForeignKey(Template, on_delete=models.CASCADE)

    message = models.TextField()
    sender = models.CharField(max_length=100)
    subject = models.TextField(max_length=250, null=True, blank=True)
    recipients = models.TextField()
    extra = models.JSONField(default=dict, blank=True)

    unit_cost = models.DecimalField(default=0, decimal_places=4, max_digits=10)
    currency_code = models.CharField(max_length=20, null=True, blank=True)
    message_count = models.PositiveSmallIntegerField(default=1)
    is_foreign = models.BooleanField(default=False)

    # cc_list = models.TextField(null=True, blank=True)
    # bcc_list = models.TextField(null=True, blank=True)
    reference = models.CharField(max_length=250, null=True, blank=True)
    response_is_valid = models.BooleanField(default=False)
    response_code = models.CharField(max_length=50)
    response_message = models.TextField()
    response_body = models.JSONField(default=dict, blank=True)

    related_message = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)
    relation_type = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return '%s: %s' % (self.pk, self.date_created)

    def save(self, *args, **kwargs):
        self.message_count = self.template.estimated_message_count
        self.is_foreign = self.get_is_foreign()
        super().save(*args, **kwargs)

    def get_is_foreign(self):
        if self.template.medium == KmsgK.MEDIUM_SMS.value:
            country_code = get_dotted_dict(self.setting.settings, 'message.country_code')
            phone = str(self.recipients)
            phone = phone[1:] if phone.startswith('+') else phone
            return not phone.startswith(str(country_code))
        return False
