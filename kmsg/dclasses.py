import decimal
from dataclasses import dataclass, field, asdict
from typing import List


@dataclass
class Response:
    is_valid: bool
    code: str
    message: str
    ref_code: str = ''
    unit_cost: decimal.Decimal = 0
    currency_code: str = str

    def get_dict(self):
        return asdict(self)

    @classmethod
    def success(cls, **kwargs):
        kwargs['is_valid'] = True
        kwargs['code'] = str(kwargs.get('code', 0))
        kwargs['message'] = kwargs.get('message') or 'Request completed successfully'
        return Response(**kwargs)

    @classmethod
    def failure(cls, **kwargs):
        kwargs['is_valid'] = False
        kwargs['code'] = str(kwargs.get('code', -1))
        kwargs['message'] = kwargs.get('message') or 'An error occurred while handling your request'
        return Response(**kwargs)


class Attachments:
    filename: str
    content: str
    content_type: str


@dataclass
class BaseMessageData:
    sender: str
    message: str
    recipients: List[str]


@dataclass
class EmailMessageData(BaseMessageData):
    subject: str
    senderName: str = ''
    cc: List[str] = field(default_factory=list)
    bcc: List[str] = field(default_factory=list)
    attachments: List[Attachments] = field(default_factory=list)


@dataclass
class SmsMessageData(BaseMessageData):
    pass


# @dataclass
# class TemplateData:
#     code: str
#     state: str
#     template_path: str = ''
#     extra_context: dict = field(default=dict)
