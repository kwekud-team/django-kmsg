from django.contrib import admin

from kmsg.models import Provider, Setting, Template, MessageLog
from kmsg.attrs import ProviderAttr, SettingAttr, TemplateAttr, MessageLogAttr
from kmsg.forms import SettingAdminForm, TemplateAdminForm


class BaseAdmin(admin.ModelAdmin):

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user

        obj.modified_by = request.user
        obj.save()
        return obj

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)

        for obj in formset.deleted_objects:
            obj.delete()

        for obj in instances:
            if not obj.date_created:
                obj.created_by = request.user

            obj.modified_by = request.user
            obj.save()
        formset.save_m2m()


@admin.register(Provider)
class ProviderAdmin(BaseAdmin):
    list_display = ProviderAttr.admin_list_display
    list_filter = ProviderAttr.admin_list_filter
    fieldsets = ProviderAttr.admin_fieldsets


@admin.register(Setting)
class SettingAdmin(BaseAdmin):
    list_display = SettingAttr.admin_list_display
    list_filter = SettingAttr.admin_list_filter
    fieldsets = SettingAttr.admin_fieldsets
    form = SettingAdminForm
    list_editable = ['priority', 'is_active']


@admin.register(Template)
class TemplateAdmin(BaseAdmin):
    list_display = TemplateAttr.admin_list_display
    list_filter = TemplateAttr.admin_list_filter
    fieldsets = TemplateAttr.admin_fieldsets
    form = TemplateAdminForm


@admin.register(MessageLog)
class MessageLogAdmin(BaseAdmin):
    list_display = MessageLogAttr.admin_list_display
    list_filter = MessageLogAttr.admin_list_filter
    fieldsets = MessageLogAttr.admin_fieldsets
    search_fields = MessageLogAttr.search_fields
    readonly_fields = MessageLogAttr.readonly_fields
