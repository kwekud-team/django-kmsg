from django.conf import settings
from kmsg.constants import KmsgK


KMSG_EMAIL_BACKENDS = getattr(settings, 'KMSG_EMAIL_BACKENDS', [
    KmsgK.BCK_MAIL_CONSOLE.value,
    KmsgK.BCK_MAIL_DJANGO_SMTP.value
])


KMSG_SMS_BACKENDS = getattr(settings, 'KMSG_SMS_BACKENDS', [
    KmsgK.BCK_SMS_HUBTEL.value,
    KmsgK.BCK_SMS_TWILIO.value
])
