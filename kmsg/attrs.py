
class ProviderAttr:
    admin_list_display = ['identifier', 'name', 'description', 'site', 'retry_with_backup', 'backup_provider', 'validate_recipients',
                          'is_active']
    admin_list_filter = ['is_active', 'retry_with_backup']
    admin_fieldsets = [
        ('Basic', {'fields': ('site', 'identifier', 'name')}),
        ('Extra', {'fields': ['description', 'retry_with_backup', 'validate_recipients', 'backup_provider',]}),
    ]


class SettingAttr:
    admin_list_display = ['pk', 'provider', 'medium', 'backend', 'priority', 'site', 'is_active']
    admin_list_filter = ['backend', 'priority', 'is_active']
    admin_fieldsets = [
        ('Basic', {'fields': ('site', 'provider', 'medium', 'backend', 'priority')}),
        ('Extra', {'fields': ['settings']}),
    ]


class TemplateAttr:
    admin_list_display = ['site', 'medium', 'identifier', 'subject', 'sender', 'estimated_message_count']
    admin_list_filter = ['date_created']
    admin_fieldsets = [
        ('Basic', {'fields': ('site', 'medium', 'identifier', 'message', 'subject')}),
        ('Extra', {'fields': ['sender', 'estimated_message_count', 'extra']}),
    ]


class MessageLogAttr:
    admin_list_display = ['date_created', 'template', 'response_code', 'message_count', 'sender', 'recipients', 'reference', 'subject',
                          'unit_cost', 'currency_code', 'created_by']
    admin_list_filter = ['date_created', 'response_is_valid', 'setting', 'setting__provider', 'template', 'is_foreign']
    admin_fieldsets = [
        ('Basic', {'fields': ('setting', 'template', 'message', 'sender', 'recipients', 'unit_cost', 'currency_code')}),
        ('Extra', {'fields': ['response_is_valid', 'response_code', 'response_message', 'response_body', 'related_message', 'relation_type',
                              'is_foreign', 'message_count']}),
    ]
    search_fields = ['recipients']
    readonly_fields = ['related_message']
