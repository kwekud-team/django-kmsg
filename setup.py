import setuptools

long_description = ''
try:
    with open("README.md", "r") as fh:
        long_description = fh.read()
except FileNotFoundError:
    pass

setuptools.setup(
     name='kmsg',
     version='0.1.14',
     author="KDanso",
     author_email="dev@kkdanso.com",
     description="SMS backend",
     long_description=long_description,
     long_description_content_type="text/markdown",
     url="https://bitbucket.org/kwekud-team/django-kmsg",
     packages=setuptools.find_packages(),
     include_package_data=True,
     classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
     ],
     install_requires=[
         'phonenumbers>=8.12.12',
         'requests>=2.24.0',
         'twilio>=7.8.0'
     ]
)
